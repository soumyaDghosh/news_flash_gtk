using Gtk 4.0;
using Adw 1;

template $ArticleViewColumn : Box {
  orientation: vertical;

  Adw.BreakpointBin {
    width-request: 300;
    height-request: 300;

    Adw.ToolbarView toolbar_view {
      reveal-bottom-bars: false;

      [top]
      Adw.HeaderBar headerbar {
        title-widget: Box {};

        [start]
        ToggleButton mark_article_button {
          sensitive: false;
          tooltip-text: _("Toggle Starred");

          Stack mark_article_stack {
            transition-duration: 50;
            transition-type: crossfade;

            StackPage {
              name: "unmarked";
              title: _("unmarked");
              child: Image {
                icon-name: "unmarked-symbolic";
              };
            }

            StackPage {
              name: "marked";
              title: _("marked");
              child: Image {
                icon-name: "marked-symbolic";
              };
            }
          }

          styles [
            "flat",
          ]
        }

        [start]
        ToggleButton mark_article_read_button {
          sensitive: false;
          tooltip-text: _("Toggle Read");

          Stack mark_article_read_stack {
            transition-duration: 50;
            transition-type: crossfade;

            StackPage {
              name: "read";
              title: _("read");
              child: Image {
                icon-name: "read-symbolic";
              };
            }

            StackPage {
              name: "unread";
              title: _("unread");
              child: Image {
                icon-name: "unread-symbolic";
              };
            }
          }

          styles [
            "flat",
          ]
        }

        [end]
        Stack more_actions_stack {
          tooltip-text: _("Article Menu");

          StackPage {
            name: "button";
            title: "button";
            child: MenuButton more_actions_button {
              valign: center;
              icon-name: "view-more-symbolic";
            };
          }

          StackPage {
            name: "spinner";
            title: "spinner";
            child: Spinner {
              valign: center;
              halign: center;
              spinning: true;
            };
          }
        }

        [end]
        Box header_box {
          spacing: 5;

          [start]
          Stack scrap_content_stack {
            transition-duration: 50;
            transition-type: crossfade;

            StackPage {
              name: "button";
              title: "button";
              child: ToggleButton scrap_content_button {
                sensitive: false;
                valign: center;
                icon-name: "accessories-dictionary-symbolic";
                tooltip-text: _("Try to Show Full Content");
              };
            }

            StackPage {
              name: "spinner";
              title: "spinner";
              child: Spinner {
                valign: center;
                halign: center;
                spinning: true;
              };
            }
          }

          [end]
          MenuButton tag_button {
            sensitive: false;
            valign: center;
            icon-name: "tag-symbolic";
            tooltip-text: _("Tag Article");

            GestureClick tag_button_click {}
          }

          $EnclosureButton enclosure_button {
            visible: false;
          }

          MenuButton share_button {
            sensitive: false;
            valign: center;
            icon-name: "emblem-shared-symbolic";
            tooltip-text: _("Share To");
          }
        }
      }

      $ArticleView article_view {}

      [bottom]
      Box {
        orientation: vertical;
        height-request: 47;

        Separator {}

        Box {
          spacing: 5;
          vexpand: true;
          halign: end;
          margin-start: 10;
          margin-end: 10;
          margin-top: 5;
          margin-bottom: 5;

          Stack footer_scrap_content_stack {
            transition-duration: 50;
            transition-type: crossfade;

            StackPage {
              name: "button";
              title: "button";
              child: ToggleButton footer_scrap_content_button {
                sensitive: false;
                icon-name: "accessories-dictionary-symbolic";
                tooltip-text: _("Try to Show Full Content");
                styles ["flat"]
              };
            }

            StackPage {
              name: "spinner";
              title: "spinner";
              child: Spinner {
                valign: center;
                halign: center;
                spinning: true;
              };
            }
          }

          MenuButton footer_tag_button {
            sensitive: false;
            icon-name: "tag-symbolic";
            tooltip-text: _("Tag Article");
            styles ["flat"]

            GestureClick footer_tag_button_click {}
          }

          $EnclosureButton footer_enclosure_button {
            visible: false;
          }

          MenuButton footer_share_button {
            icon-name: "emblem-shared-symbolic";
            tooltip-text: _("Share To");
            styles ["flat"]
          }
        }
      }
    }

    Adw.Breakpoint {
      condition ("max-width: 500px")
      setters {
        header_box.visible: false;
        toolbar_view.reveal-bottom-bars: true;
        article_view.status-margin: 0;
      }
    }
  }
}


