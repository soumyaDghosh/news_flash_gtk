use std::{collections::HashSet, time::Duration};

use chrono::{DateTime, Utc};
use gio::SimpleAction;
use glib::{subclass::prelude::*, VariantType};
use gtk4::prelude::*;
use libadwaita::prelude::*;
use news_flash::{
    error::NewsFlashError,
    models::{ArticleID, CategoryID, FeedID, FeedMapping, TagID, Url},
};
use tokio::time::timeout;

use crate::{
    about_dialog::NewsFlashAbout,
    add_dialog::{AddCategoryDialog, AddFeedDialog, AddTagDialog},
    app::{App, NotificationCounts},
    article_list::{MarkUpdate, ReadUpdate},
    content_page::ArticleListMode,
    edit_category_dialog::EditCategoryDialog,
    edit_feed_dialog::EditFeedDialog,
    edit_tag_dialog::EditTagDialog,
    error_dialog::ErrorDialog,
    i18n::i18n,
    settings::{Keybindings, SettingsDialog},
    shortcuts_dialog::ShortcutsDialog,
    undo_delete_action::UndoDelete,
    util::constants,
};

pub struct MainWindowActions;

impl MainWindowActions {
    pub fn setup() {
        let main_window = App::default().main_window();

        // -------------------------
        // sync
        // -------------------------
        let sync_action = SimpleAction::new("sync", None);
        sync_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .start_sync();
            App::default().content_page_state().borrow_mut().started_sync();
            App::set_background_status(constants::BACKGROUND_SYNC);
            Self::update_state();

            App::default().execute_with_callback(
                |news_flash, client| async move {
                    if let Some(news_flash) = news_flash.read().await.as_ref() {
                        let last_sync_datetime = news_flash.last_sync().await;

                        let new_article_count = news_flash.sync(&client).await?;
                        let unread_total = news_flash.unread_count_all().unwrap_or(0);
                        let feed_ids = news_flash
                            .get_feeds()
                            .map(|(feeds, _mappings)| feeds.into_iter().map(|f| f.feed_id).collect::<HashSet<FeedID>>())
                            .unwrap_or_else(|_| HashSet::new());
                        Ok((new_article_count, unread_total, feed_ids, last_sync_datetime))
                    } else {
                        Err(NewsFlashError::NotLoggedIn)
                    }
                },
                |app, res| {
                    app.content_page_state().borrow_mut().finished_sync();

                    match res {
                        Ok((new_article_count, unread_total, feed_ids, last_sync_datetime)) => {
                            _ = app.settings().write().delete_old_feed_settings(&feed_ids);

                            app.main_window().content_page().article_list_column().finish_sync();
                            App::default().update_sidebar();
                            App::default().update_article_list();
                            let counts = NotificationCounts {
                                new: new_article_count,
                                unread: unread_total,
                            };
                            app.show_notification(counts);
                            Self::scrap_content_feeds(last_sync_datetime, feed_ids);
                        }
                        Err(error) => {
                            app.main_window().content_page().article_list_column().finish_sync();
                            App::default().in_app_error(&i18n("Failed to sync"), error);

                            App::set_background_status(constants::BACKGROUND_IDLE);
                            Self::update_state();
                        }
                    }
                },
            );
        });

        // -------------------------
        // edit category dialog
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let edit_category_dialog_action = SimpleAction::new("edit-category-dialog", Some(&type_));
        edit_category_dialog_action.connect_activate(|_action, parameter| {
            if let Some(category_id_str) = parameter.and_then(|p| p.str()) {
                let category_id = CategoryID::new(category_id_str);

                App::default().execute_with_callback(
                    |news_flash, _client| async move {
                        if let Some(news_flash) = news_flash.read().await.as_ref() {
                            let categories = news_flash.get_categories().map(|(c, _m)| c).ok()?;
                            let category = categories.into_iter().find(|c| c.category_id == category_id)?;
                            Some(category)
                        } else {
                            None
                        }
                    },
                    |app, res| {
                        if let Some(category) = res {
                            EditCategoryDialog::new(category).present(&app.main_window());
                        } else {
                            log::error!("Failed to find category for edit dialog");
                        }
                    },
                );
            } else {
                App::default().in_app_notifiaction(&i18n("Edit category: no parameter"));
            }
        });

        // -------------------------
        // delete category
        // -------------------------
        let type_ = VariantType::new("(ss)").unwrap();
        let delete_category_action = SimpleAction::new("enqueue-delete-category", Some(&type_));
        delete_category_action.connect_activate(|_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let category_id = CategoryID::new(id_str);
                    App::default()
                        .main_window()
                        .show_undo_toast(UndoDelete::Category(category_id, name_str.into()));
                }
            } else {
                App::default().in_app_notifiaction(&i18n("Delete category: no parameter"));
            }
        });

        // -------------------------
        // edit feed dialog
        // -------------------------
        let type_ = VariantType::new("(ss)").unwrap();
        let edit_feed_dialog_action = SimpleAction::new("edit-feed-dialog", Some(&type_));
        edit_feed_dialog_action.connect_activate(|_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let parent_value = parameter.child_value(1);

                if let (Some(id_str), Some(parent_str)) = (id_value.str(), parent_value.str()) {
                    let feed_id = FeedID::new(id_str);
                    let parent_id = CategoryID::new(parent_str);
                    let feed_mapping = FeedMapping {
                        feed_id: feed_id.clone(),
                        category_id: parent_id,
                        sort_index: None,
                    };

                    App::default().execute_with_callback(
                        |news_flash, _client| async move {
                            if let Some(news_flash) = news_flash.read().await.as_ref() {
                                let categories = news_flash.get_categories().map(|(c, _m)| c).ok()?;
                                let feeds = news_flash.get_feeds().map(|(f, _m)| f).ok()?;
                                let feed = feeds.into_iter().find(|f| f.feed_id == feed_id)?;
                                Some((feed, categories))
                            } else {
                                None
                            }
                        },
                        move |app, res| {
                            if let Some((feed, categories)) = res {
                                EditFeedDialog::new(feed, feed_mapping, categories).present(&app.main_window());
                            } else {
                                log::error!("Failed to find feed for edit dialog");
                            }
                        },
                    );
                }
            } else {
                App::default().in_app_notifiaction(&i18n("Edit feed: no parameter"));
            }
        });

        // -------------------------
        // delete feed
        // -------------------------
        let type_ = VariantType::new("(ss)").unwrap();
        let delete_feed_action = SimpleAction::new("enqueue-delete-feed", Some(&type_));
        delete_feed_action.connect_activate(|_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let feed_id = FeedID::new(id_str);
                    App::default()
                        .main_window()
                        .show_undo_toast(UndoDelete::Feed(feed_id, name_str.into()));
                }
            } else {
                App::default().in_app_notifiaction(&i18n("Delete feed: no parameter"));
            }
        });

        // -------------------------
        // edit tag dialog
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let edit_tag_dialog_action = SimpleAction::new("edit-tag-dialog", Some(&type_));
        edit_tag_dialog_action.connect_activate(|_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let tag_id = TagID::new(id_str);

                App::default().execute_with_callback(
                    |news_flash, _client| async move {
                        if let Some(news_flash) = news_flash.read().await.as_ref() {
                            let tags = news_flash.get_tags().map(|(tags, _taggings)| tags).ok()?;
                            let tag = tags.into_iter().find(|t| t.tag_id == tag_id)?;
                            Some(tag)
                        } else {
                            None
                        }
                    },
                    |app, res| {
                        if let Some(tag) = res {
                            EditTagDialog::new(tag).present(&app.main_window());
                        } else {
                            log::error!("Failed to find tag for edit dialog");
                        }
                    },
                );
            }
        });

        // -------------------------
        // delete tag
        // -------------------------
        let type_ = VariantType::new("(ss)").unwrap();
        let delete_tag_action = SimpleAction::new("enqueue-delete-tag", Some(&type_));
        delete_tag_action.connect_activate(|_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let tag_id = TagID::new(id_str);
                    App::default()
                        .main_window()
                        .show_undo_toast(UndoDelete::Tag(tag_id, name_str.into()));
                }
            } else {
                App::default().in_app_notifiaction(&i18n("Delete Tag: no parameter"));
            }
        });

        // -------------------------
        // mark feed as read
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let mark_feed_read_action = SimpleAction::new("mark-feed-read", Some(&type_));
        mark_feed_read_action.connect_activate(|_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let feed_id = FeedID::new(id_str);
                App::set_feed_read(&feed_id);
            }
        });

        // -------------------------
        // mark category as read
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let mark_category_read_action = SimpleAction::new("mark-category-read", Some(&type_));
        mark_category_read_action.connect_activate(|_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let category_id = CategoryID::new(id_str);
                App::set_category_read(&category_id);
            }
        });

        // -------------------------
        // mark tag as read
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let mark_tag_read_action = SimpleAction::new("mark-tag-read", Some(&type_));
        mark_tag_read_action.connect_activate(|_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let tag_id = TagID::new(id_str);
                App::set_tag_read(&tag_id);
            }
        });

        // -------------------------
        // show shortcuts dialog
        // -------------------------
        let show_shortcut_window_action = SimpleAction::new("shortcut-window", None);
        show_shortcut_window_action.connect_activate(|_action, _parameter| {
            let dialog = ShortcutsDialog::new(&App::default().main_window(), &App::default().settings().read());
            dialog.widget.present();
        });

        // -------------------------
        // about dialog
        // -------------------------
        let show_about_window_action = SimpleAction::new("about-window", None);
        show_about_window_action.connect_activate(|_action, _parameter| {
            let app = App::default();
            NewsFlashAbout::show(&app.main_window());
        });

        // -------------------------
        // show settings dialog
        // -------------------------
        let settings_window_action = SimpleAction::new("settings", None);
        settings_window_action.connect_activate(|_action, _parameter| {
            SettingsDialog::new().present(&App::default().main_window());
        });

        // -------------------------
        // show discover dialog
        // -------------------------
        let discover_dialog_action = SimpleAction::new("discover", None);
        discover_dialog_action.connect_activate(|_action, _parameter| {
            App::default().spawn_discover_dialog();
        });

        // -------------------------
        // add tag dialog
        // -------------------------
        let add_tag_action = SimpleAction::new("add-tag", None);
        add_tag_action
            .connect_activate(|_action, _parameter| AddTagDialog::new().present(&App::default().main_window()));

        // -------------------------
        // add category dialog
        // -------------------------
        let add_category_action = SimpleAction::new("add-category", None);
        add_category_action
            .connect_activate(|_action, _parameter| AddCategoryDialog::new().present(&App::default().main_window()));

        // -------------------------
        // add feed dialog
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let add_feed_action = SimpleAction::new("add-feed", Some(&type_));
        add_feed_action.connect_activate(|_action, parameter| {
            let dialog = AddFeedDialog::new();
            dialog.present(&App::default().main_window());

            if let Some(subscribe_url) = parameter
                .and_then(|p| p.str().map(String::from))
                .filter(|url_str| !url_str.is_empty())
            {
                dialog.parse_url_str(&subscribe_url);
            }
        });

        // -------------------------
        // quit app
        // -------------------------
        let quit_action = SimpleAction::new("quit-application", None);
        quit_action.connect_activate(|_action, _parameter| {
            App::default().queue_quit();
        });

        // -------------------------
        // import opml
        // -------------------------
        let import_opml_action = SimpleAction::new("import-opml", None);
        import_opml_action.connect_activate(|_action, _parameter| {
            App::default().import_opml();
        });

        // -------------------------
        // export opml
        // -------------------------
        let export_opml_action = SimpleAction::new("export-opml", None);
        export_opml_action.connect_activate(|_action, _parameter| {
            App::default().export_opml();
        });

        // -------------------------
        // save image
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let save_image_action = SimpleAction::new("save-webview-image", Some(&type_));
        save_image_action.connect_activate(|_action, parameter| {
            if let Some(image_uri) = parameter.and_then(|p| p.str()) {
                App::default().save_image(image_uri);
            } else {
                App::default().in_app_notifiaction(&i18n("save webview image: no parameter"));
            }
        });

        // -------------------------
        // reset account
        // -------------------------
        let reset_account_action = SimpleAction::new("reset-account", None);
        reset_account_action.connect_activate(|_action, _parameter| App::default().main_window().show_reset_page());

        // -------------------------
        // update account login
        // -------------------------
        let update_login_action = SimpleAction::new("update-login", None);
        update_login_action.connect_activate(|_action, _parameter| {
            App::default().update_login();
        });

        // -------------------------
        // close article
        // -------------------------
        let close_article_action = SimpleAction::new("close-article", None);
        close_article_action.connect_activate(|_action, _parameter| {
            let main_window = App::default().main_window();
            let article_view_column = main_window.content_page().articleview_column();

            // clear view
            article_view_column.article_view().close_article();
            // update headerbar
            article_view_column.clear_article();
        });
        close_article_action.set_enabled(false);

        // -------------------------
        // fullscreen article
        // -------------------------
        let fullscreen_article_action = SimpleAction::new("fullscreen-article", None);
        fullscreen_article_action.connect_activate(|_action, _parameter| {
            let main_window = App::default().main_window();
            let is_fullscreened = main_window.is_fullscreened();
            main_window.set_fullscreened(!is_fullscreened);
            if let Some(article_navigation_page) = main_window.content_page().inner().content() {
                article_navigation_page.set_can_pop(is_fullscreened);
            }
        });

        // -------------------------
        // export article
        // -------------------------
        let export_article_action = SimpleAction::new("export-article", None);
        export_article_action.connect_activate(|_action, _parameter| {
            App::default().export_article();
        });
        export_article_action.set_enabled(false);

        // -------------------------
        // open article in browser
        // -------------------------
        let open_selected_article_action = SimpleAction::new("open-selected-article-in-browser", None);
        open_selected_article_action.connect_activate(|_action, _parameter| {
            App::default().open_selected_article_in_browser();
        });
        open_selected_article_action.set_enabled(false);

        // -------------------------
        // open uri in browser
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let open_uri_action = SimpleAction::new("open-uri-in-browser", Some(&type_));
        open_uri_action.connect_activate(|_action, parameter| {
            if let Some(uri) = parameter.and_then(|p| p.str()) {
                if let Ok(uri) = Url::parse(uri) {
                    App::default().open_url_in_default_browser(&uri);
                }
            } else {
                App::default().in_app_notifiaction(&i18n("open uri: no parameter"));
            }
        });

        let type_ = VariantType::new("s").unwrap();
        let open_article_action = SimpleAction::new("open-article-in-browser", Some(&type_));
        open_article_action.connect_activate(|_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let article_id = ArticleID::new(id_str);
                App::default().open_article_in_browser(&article_id);
            }
        });

        // -------------------------
        // toggle article read
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let toggle_article_read_action = SimpleAction::new("toggle-article-read", Some(&type_));
        toggle_article_read_action.connect_activate(|_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let article_id = ArticleID::new(id_str);
                if let Some(article_model) = App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .article_list()
                    .get_model(&article_id)
                {
                    App::default().mark_article_read(ReadUpdate {
                        article_id: article_id.clone(),
                        read: article_model.read().invert().into(),
                    });
                } else {
                    log::warn!("article '{:?}' not part of article-list", article_id);
                }
            }
        });

        // -------------------------
        // toggle article marked
        // -------------------------
        let type_ = VariantType::new("s").unwrap();
        let toggle_article_marked_action = SimpleAction::new("toggle-article-marked", Some(&type_));
        toggle_article_marked_action.connect_activate(|_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let article_id = ArticleID::new(id_str);
                if let Some(article_model) = App::default()
                    .main_window()
                    .content_page()
                    .article_list_column()
                    .article_list()
                    .get_model(&article_id)
                {
                    App::default().mark_article(MarkUpdate {
                        article_id: article_id.clone(),
                        marked: article_model.marked().invert().into(),
                    });
                } else {
                    log::warn!("article '{:?}' not part of article-list", article_id);
                }
            }
        });

        // -------------------------
        // show error dialog
        // -------------------------
        let show_error_dialog_action = SimpleAction::new("show-error-dialog", None);
        show_error_dialog_action.connect_activate(|_action, _parameter| {
            let app = App::default();
            if let Some(error) = app.imp().news_flash_error.take() {
                ErrorDialog::new(&error).present(&app.main_window());
            }
        });

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let remove_undo_delete_action = SimpleAction::new("remove-undo-action", None);
        remove_undo_delete_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .remove_current_undo_delete_action();
        });

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let undo_mark_all_read_action = SimpleAction::new("undo-mark-all-read", None);
        undo_mark_all_read_action.connect_activate(|_action, _parameter| {
            if let Some(current_undo_mark_read_action) = App::default()
                .main_window()
                .content_page()
                .imp()
                .current_undo_mark_read_action
                .take()
            {
                App::set_articles_unread(current_undo_mark_read_action.article_ids);
            }
        });

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let ignore_tls_errors_action = SimpleAction::new("ignore-tls-errors", None);
        ignore_tls_errors_action.connect_activate(|_action, _parameter| {
            let settings = App::default().settings();
            let res1 = settings.write().set_accept_invalid_certs(true);
            let res2 = settings.write().set_accept_invalid_hostnames(true);
            if res1.is_err() || res2.is_err() {
                App::default().in_app_notifiaction(&i18n("Error writing settings"));
            }
        });

        // ---------------------------
        // focus search action
        // ---------------------------
        let focus_search_action = SimpleAction::new("focus-search", None);
        focus_search_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .focus_search();
        });

        // ---------------------------
        // next article
        // ---------------------------
        let next_article_action = SimpleAction::new("next-article", None);
        next_article_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_next_article()
        });

        // ---------------------------
        // prev article
        // ---------------------------
        let prev_article_action = SimpleAction::new("prev-article", None);
        prev_article_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_prev_article()
        });

        // ---------------------------
        // scroll down
        // ---------------------------
        let scroll_down_action = SimpleAction::new("scroll-down", None);
        scroll_down_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_view_scroll_diff(150.0)
        });

        // ---------------------------
        // scroll up
        // ---------------------------
        let scroll_up_action = SimpleAction::new("scroll-up", None);
        scroll_up_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_view_scroll_diff(-150.0)
        });

        // ---------------------------
        // show all articles
        // ---------------------------
        let all_articles_action = SimpleAction::new("all-articles", None);
        all_articles_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .set_view_switcher_stack(ArticleListMode::All)
        });

        // ---------------------------
        // show only unread
        // ---------------------------
        let only_unread_action = SimpleAction::new("only-unread", None);
        only_unread_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .set_view_switcher_stack(ArticleListMode::Unread)
        });

        // ---------------------------
        // show only starred
        // ---------------------------
        let only_starred_action = SimpleAction::new("only-starred", None);
        only_starred_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .set_view_switcher_stack(ArticleListMode::Marked)
        });

        // ---------------------------
        // toggle category expanded
        // ---------------------------
        let toggle_category_expanded_action = SimpleAction::new("toggle-category-expanded", None);
        toggle_category_expanded_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .sidebar_column()
                .sidebar()
                .expand_collapse_selected_category()
        });

        // ---------------------------
        // set sidebar read
        // ---------------------------
        let set_sidebar_read_action = SimpleAction::new("set-sidebar-read", None);
        set_sidebar_read_action
            .connect_activate(|_action, _parameter| App::default().main_window().content_page().set_sidebar_read());

        // ---------------------------
        // sidebar next
        // ---------------------------
        let sidebar_next_action = SimpleAction::new("sidebar-next", None);
        sidebar_next_action.connect_activate(|_action, _parameter| {
            App::default().main_window().content_page().sidebar_select_next_item()
        });

        // ---------------------------
        // sidebar prev
        // ---------------------------
        let sidebar_prev_action = SimpleAction::new("sidebar-prev", None);
        sidebar_prev_action.connect_activate(|_action, _parameter| {
            App::default().main_window().content_page().sidebar_select_prev_item()
        });

        // ---------------------------
        // copy selected article url
        // ---------------------------
        let copy_article_url_action = SimpleAction::new("copy-article-url", None);
        copy_article_url_action.connect_activate(|_action, _parameter| {
            let main_window = App::default().main_window();
            let visible_article = main_window.content_page().visible_article_manager().get_article();

            if let Some(article_model) = visible_article {
                if let Some(url) = article_model.url {
                    main_window.clipboard().set_text(url.as_str());
                    main_window
                        .content_page()
                        .simple_message(&i18n("Article URL copied to clipboard"));
                } else {
                    log::warn!("Copy article url to clipboard: No url available.")
                }
            } else {
                log::warn!("Copy article url to clipboard: No article Selected.")
            }
        });

        // ---------------------------
        // scrap article content
        // ---------------------------
        let scrap_content_action = SimpleAction::new("scrap-content", None);
        scrap_content_action.connect_activate(|_action, _parameter| {
            let article = match App::default()
                .main_window()
                .content_page()
                .visible_article_manager()
                .get_article()
            {
                Some(article) => article,
                _ => return,
            };

            // Article already scraped: just swap to scraped content
            if article.scraped_content.is_some() {
                App::default()
                    .content_page_state()
                    .borrow_mut()
                    .set_prefer_scraped_content(true);
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .redraw_article();
                return;
            }

            App::default()
                .content_page_state()
                .borrow_mut()
                .started_scraping_article();
            App::default()
                .main_window()
                .content_page()
                .articleview_column()
                .start_scrap_content_spinner();

            Self::update_state();

            let article_id = article.article_id;
            App::default().execute_with_callback(
                |news_flash, client| async move {
                    if let Some(news_flash) = news_flash.read().await.as_ref() {
                        let news_flash_future = news_flash.scrap_content_article(&article_id, &client, None);
                        timeout(Duration::from_secs(60), news_flash_future).await
                    } else {
                        Ok(Err(NewsFlashError::NotLoggedIn))
                    }
                },
                |app, res| {
                    app.main_window()
                        .content_page()
                        .articleview_column()
                        .stop_scrap_content_spinner();

                    app.content_page_state().borrow_mut().finished_scraping_article();
                    Self::update_state();

                    match res {
                        Ok(Ok(article)) => {
                            let scraped_article_id = article.article_id;
                            let visible_article_id = app.content_page_state().borrow().get_visible_article_id();

                            if visible_article_id.map(|id| id == scraped_article_id).unwrap_or(false) {
                                app.main_window()
                                    .content_page()
                                    .visible_article_manager()
                                    .load_article_from_db(scraped_article_id, false);
                            }

                            if article.thumbnail_url.is_some() {
                                app.update_article_list();
                            }
                        }
                        Ok(Err(error)) => {
                            log::warn!("Internal scraper failed: {error}");
                            App::default().in_app_error(&i18n("Scraper failed to extract content"), error);
                        }
                        Err(_error) => {
                            log::warn!("Internal scraper elapsed");
                            App::default().in_app_notifiaction(&i18n("Scraper Timeout"));
                        }
                    }
                },
            );
        });

        // ---------------------------
        // tag article
        // ---------------------------
        let tag_article_action = SimpleAction::new("tag-article", None);
        tag_article_action.connect_activate(|_action, _parameter| {
            App::default()
                .main_window()
                .content_page()
                .articleview_column()
                .popup_tag_popover()
        });

        // ---------------------------
        // toggle selected article read
        // ---------------------------
        let toggle_read_action = SimpleAction::new("toggle-read", None);
        toggle_read_action.connect_activate(|_action, _parameter| App::default().toggle_selected_article_read());

        // ---------------------------
        // toggle selected article marked
        // ---------------------------
        let toggle_marked_action = SimpleAction::new("toggle-marked", None);
        toggle_marked_action.connect_activate(|_action, _parameter| App::default().toggle_selected_article_marked());

        main_window.add_action(&sync_action);
        main_window.add_action(&delete_category_action);
        main_window.add_action(&edit_category_dialog_action);
        main_window.add_action(&delete_feed_action);
        main_window.add_action(&delete_tag_action);
        main_window.add_action(&edit_feed_dialog_action);
        main_window.add_action(&edit_tag_dialog_action);
        main_window.add_action(&mark_feed_read_action);
        main_window.add_action(&mark_category_read_action);
        main_window.add_action(&mark_tag_read_action);
        main_window.add_action(&show_shortcut_window_action);
        main_window.add_action(&show_about_window_action);
        main_window.add_action(&settings_window_action);
        main_window.add_action(&discover_dialog_action);
        main_window.add_action(&add_tag_action);
        main_window.add_action(&add_category_action);
        main_window.add_action(&add_feed_action);
        main_window.add_action(&quit_action);
        main_window.add_action(&import_opml_action);
        main_window.add_action(&export_opml_action);
        main_window.add_action(&save_image_action);
        main_window.add_action(&reset_account_action);
        main_window.add_action(&update_login_action);
        main_window.add_action(&close_article_action);
        main_window.add_action(&fullscreen_article_action);
        main_window.add_action(&open_selected_article_action);
        main_window.add_action(&open_uri_action);
        main_window.add_action(&open_article_action);
        main_window.add_action(&export_article_action);
        main_window.add_action(&toggle_article_read_action);
        main_window.add_action(&toggle_article_marked_action);
        main_window.add_action(&show_error_dialog_action);
        main_window.add_action(&remove_undo_delete_action);
        main_window.add_action(&undo_mark_all_read_action);
        main_window.add_action(&ignore_tls_errors_action);
        main_window.add_action(&focus_search_action);
        main_window.add_action(&next_article_action);
        main_window.add_action(&prev_article_action);
        main_window.add_action(&scroll_down_action);
        main_window.add_action(&scroll_up_action);
        main_window.add_action(&all_articles_action);
        main_window.add_action(&only_unread_action);
        main_window.add_action(&only_starred_action);
        main_window.add_action(&toggle_category_expanded_action);
        main_window.add_action(&set_sidebar_read_action);
        main_window.add_action(&sidebar_next_action);
        main_window.add_action(&sidebar_prev_action);
        main_window.add_action(&copy_article_url_action);
        main_window.add_action(&scrap_content_action);
        main_window.add_action(&tag_article_action);
        main_window.add_action(&toggle_read_action);
        main_window.add_action(&toggle_marked_action);

        let app = App::default();
        app.set_accels_for_action("win.settings", &["<primary>comma"]);
        app.set_accels_for_action("win.shortcut-window", &["<primary>question"]);
        app.set_accels_for_action("win.quit-application", &["<primary>Q"]);

        Self::update_state();
        Self::set_keybindings();
    }

    fn scrap_content_feeds(last_sync: DateTime<Utc>, feed_ids: HashSet<FeedID>) {
        let scrap_feed_ids = feed_ids
            .into_iter()
            .filter(|id| {
                App::default()
                    .settings()
                    .read()
                    .get_feed_settings(id)
                    .map(|settings| settings.scrap_content)
                    .unwrap_or(false)
            })
            .collect::<Vec<_>>();

        if scrap_feed_ids.is_empty() {
            App::set_background_status(constants::BACKGROUND_IDLE);
            Self::update_state();
            return;
        }

        App::default()
            .content_page_state()
            .borrow_mut()
            .started_scraping_article();
        App::default()
            .main_window()
            .content_page()
            .articleview_column()
            .start_scrap_content_spinner();
        App::default().content_page_state().borrow_mut().finished_sync();

        Self::update_state();

        App::default().execute_with_callback(
            move |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash
                        .scrap_content_feeds(last_sync, &scrap_feed_ids, &client)
                        .await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |app, res| {
                app.main_window()
                    .content_page()
                    .articleview_column()
                    .stop_scrap_content_spinner();

                app.content_page_state().borrow_mut().finished_scraping_article();
                app.update_article_list();

                Self::update_state();
                App::set_background_status(constants::BACKGROUND_IDLE);

                if let Err(error) = res {
                    log::warn!("Internal scraper failed: {error}");
                    App::default().in_app_error(&i18n("Scraper failed to extract content"), error);
                }
            },
        );
    }

    pub fn update_state() {
        let state = App::default().content_page_state();
        let is_article_open = state.borrow().get_visible_article_id().is_some();
        let offline = state.borrow().get_offline();
        let search_focused = state.borrow().get_search_focused();
        let content_page_visible = state.borrow().get_content_page_visible();
        let is_article_scrap_ongoing = state.borrow().is_article_scrap_ongoing();
        let is_syncing = state.borrow().is_syncing();
        let dialog_open = state.borrow().get_dialog_open();

        let block_shortcuts = !content_page_visible || search_focused || dialog_open;

        Self::action_set_enabled(
            "sync",
            !offline && !block_shortcuts && !is_syncing && !is_article_scrap_ongoing,
        );
        Self::action_set_enabled("set-sidebar-read", !offline && !block_shortcuts);
        Self::action_set_enabled("import-opml", !offline && !block_shortcuts);
        Self::action_set_enabled("discover", !offline && !block_shortcuts);

        Self::action_set_enabled("close-article", is_article_open && !block_shortcuts);
        Self::action_set_enabled("export-article", is_article_open && !block_shortcuts);
        Self::action_set_enabled("open-selected-article-in-browser", is_article_open && !block_shortcuts);

        Self::action_set_enabled("fullscreen-article", !block_shortcuts);
        Self::action_set_enabled("focus-search", !block_shortcuts);
        Self::action_set_enabled("next-article", !block_shortcuts);
        Self::action_set_enabled("prev-article", !block_shortcuts);
        Self::action_set_enabled("scroll-down", !block_shortcuts);
        Self::action_set_enabled("scroll-up", !block_shortcuts);
        Self::action_set_enabled("all-articles", !block_shortcuts);
        Self::action_set_enabled("only-unread", !block_shortcuts);
        Self::action_set_enabled("only-starred", !block_shortcuts);
        Self::action_set_enabled("copy-article-url", !block_shortcuts);
        Self::action_set_enabled("toggle-category-expanded", !block_shortcuts);
        Self::action_set_enabled("sidebar-set-read", !block_shortcuts);
        Self::action_set_enabled("sidebar-next", !block_shortcuts);
        Self::action_set_enabled("sidebar-prev", !block_shortcuts);
        Self::action_set_enabled(
            "scrap-content",
            !block_shortcuts && !is_syncing && !is_article_scrap_ongoing,
        );
        Self::action_set_enabled("tag-article", !block_shortcuts);
        Self::action_set_enabled("toggle-marked", !block_shortcuts);
        Self::action_set_enabled("toggle-read", !block_shortcuts);
        Self::action_set_enabled("close-article", !block_shortcuts);
    }

    fn action_set_enabled(name: &str, enabled: bool) {
        if let Some(action) = App::default().main_window().lookup_action(name) {
            if let Ok(simple_action) = action.downcast::<SimpleAction>() {
                simple_action.set_enabled(enabled);
            }
        }
    }

    pub fn set_keybindings() {
        Self::set_keybinding("refresh", "win.sync");
        Self::set_keybinding("fullscreen", "win.fullscreen-article");
        Self::set_keybinding("open_browser", "win.open-selected-article-in-browser");
        Self::set_keybinding("search", "win.focus-search");
        Self::set_keybinding("next_article", "win.next-article");
        Self::set_keybinding("previous_article", "win.prev-article");
        Self::set_keybinding("scroll_down", "win.scroll-down");
        Self::set_keybinding("scroll_up", "win.scroll-up");
        Self::set_keybinding("all_articles", "win.all-articles");
        Self::set_keybinding("only_unread", "win.only-unread");
        Self::set_keybinding("only_starred", "win.only-starred");
        Self::set_keybinding("copy_url", "win.copy-article-url");
        Self::set_keybinding("toggle_category_expanded", "win.toggle-category-expanded");
        Self::set_keybinding("sidebar_set_read", "win.sidebar-set-read");
        Self::set_keybinding("next_item", "win.sidebar-next");
        Self::set_keybinding("previous_item", "win.sidebar-prev");
        Self::set_keybinding("scrap_content", "win.scrap-content");
        Self::set_keybinding("tag", "win.tag-article");
        Self::set_keybinding("toggle_read", "win.toggle-read");
        Self::set_keybinding("toggle_marked", "win.toggle-marked");
        Self::set_keybinding("quit", "win.quit-application");
        Self::set_keybinding("close_article", "win.close-article");
        Self::set_keybinding("sidebar_set_read", "win.set-sidebar-read");
    }

    fn set_keybinding(id: &str, action: &str) {
        let keybinding = Keybindings::read_keybinding(id).unwrap_or(None);
        let keybinding: &str = keybinding.as_deref().unwrap_or("");
        App::default().set_accels_for_action(action, &[keybinding]);
    }
}
