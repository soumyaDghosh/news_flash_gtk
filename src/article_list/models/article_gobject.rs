use super::ArticleListArticleModel;
use crate::util::DateUtil;
use chrono::{DateTime, NaiveDateTime, Utc};
use glib::prelude::*;
use glib::{Boxed, Enum, Object, ParamSpec, ParamSpecBoxed, ParamSpecEnum, ParamSpecString, Value};
use gtk4::subclass::prelude::*;
use news_flash::models::{ArticleID, FeedID, Marked, Read, Tag};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
use std::sync::Arc;

mod imp {
    use glib::ParamSpecBoolean;

    use super::*;

    pub struct ArticleGObject {
        pub id: RefCell<ArticleID>,
        pub title: RefCell<Arc<String>>,
        pub feed_id: RefCell<FeedID>,
        pub summary: RefCell<Arc<String>>,

        pub date: RefCell<GDateTime>,
        pub date_string: RefCell<String>,
        pub read: Cell<GRead>,
        pub marked: Cell<GMarked>,
        pub tags: RefCell<GTags>,
        pub thumbnail: RefCell<GThumbnail>,
        pub feed_title: RefCell<String>,

        pub selected: Cell<bool>,
    }

    impl Default for ArticleGObject {
        fn default() -> Self {
            Self {
                id: RefCell::new(ArticleID::new("")),
                title: RefCell::new(Arc::new("".into())),
                feed_id: RefCell::new(FeedID::new("")),
                feed_title: RefCell::new("".into()),
                summary: RefCell::new(Arc::new("".into())),

                date: RefCell::new(Utc::now().naive_utc().into()),
                date_string: RefCell::new(DateUtil::format_time(&Utc::now().naive_utc())),
                read: Cell::new(GRead::Unread),
                marked: Cell::new(GMarked::Unmarked),
                tags: RefCell::new(Vec::new().into()),
                thumbnail: RefCell::new(GThumbnail::default()),

                selected: Cell::new(false),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleGObject {
        const NAME: &'static str = "NewsFlashArticleGObject";
        type Type = super::ArticleGObject;
    }

    impl ObjectImpl for ArticleGObject {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecEnum::builder::<GRead>("read").build(),
                    ParamSpecEnum::builder::<GMarked>("marked").build(),
                    ParamSpecString::builder("date-string").build(),
                    ParamSpecBoxed::builder::<GDateTime>("date").build(),
                    ParamSpecBoxed::builder::<GTags>("tags").build(),
                    ParamSpecBoxed::builder::<GThumbnail>("thumbnail").build(),
                    ParamSpecString::builder("feed-title").build(),
                    ParamSpecBoolean::builder("selected").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "read" => {
                    let input = value.get().expect("The value needs to be of type `NewsFlashGRead`.");
                    self.read.replace(input);
                }
                "marked" => {
                    let input = value.get().expect("The value needs to be of type `NewsFlashGMarked`.");
                    self.marked.replace(input);
                }
                "date" => {
                    let input = value
                        .get()
                        .expect("The value needs to be of type `NewsFlashGDateTime`.");
                    self.date.replace(input);
                }
                "date-string" => {
                    let input = value.get().expect("The value needs to be of type `string`.");
                    self.date_string.replace(input);
                }
                "tags" => {
                    let input = value.get().expect("The value needs to be of type `NewsFlashGTags`.");
                    self.tags.replace(input);
                }
                "thumbnail" => {
                    let input = value
                        .get()
                        .expect("The value needs to be of type `NewsFlashGThumbnail`.");
                    self.thumbnail.replace(input);
                }
                "feed-title" => {
                    let input = value
                        .get()
                        .expect("The value needs to be of type `NewsFlashGThumbnail`.");
                    self.feed_title.replace(input);
                }
                "selected" => {
                    let input = value.get().expect("The value needs to be of type `bool`.");
                    self.selected.set(input);
                }
                _ => unreachable!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "read" => self.read.get().to_value(),
                "marked" => self.marked.get().to_value(),
                "date" => self.date.borrow().to_value(),
                "date-string" => self.date_string.borrow().to_value(),
                "tags" => self.tags.borrow().to_value(),
                "thumbnail" => self.thumbnail.borrow().to_value(),
                "feed-title" => self.feed_title.borrow().to_value(),
                "selected" => self.selected.get().to_value(),
                _ => unreachable!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct ArticleGObject(ObjectSubclass<imp::ArticleGObject>);
}

impl Default for ArticleGObject {
    fn default() -> Self {
        Object::new()
    }
}

impl ArticleGObject {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn from_model(model: &ArticleListArticleModel) -> Self {
        let gobject = Self::new();
        let imp = gobject.imp();

        imp.id.replace(model.id.clone());
        imp.title.replace(model.title.clone());
        imp.feed_id.replace(model.feed_id.clone());
        imp.feed_title.replace(model.feed_title.clone());
        imp.summary.replace(model.summary.clone());

        imp.date.replace(model.date.into());
        imp.date_string.replace(DateUtil::format_time(&model.date));
        imp.read.replace(model.read.into());
        imp.marked.replace(model.marked.into());
        imp.tags.replace(model.tags.clone().into());
        gobject
    }

    pub fn read(&self) -> GRead {
        self.imp().read.get()
    }

    pub fn set_gread(&self, g_read: GRead) {
        self.set_property("read", g_read.to_value())
    }

    pub fn set_read(&self, read: Read) {
        let g_read: GRead = read.into();
        self.set_property("read", g_read.to_value())
    }

    pub fn marked(&self) -> GMarked {
        self.imp().marked.get()
    }

    pub fn set_gmarked(&self, g_marked: GMarked) {
        self.set_property("marked", g_marked.to_value())
    }

    pub fn set_marked(&self, marked: Marked) {
        let g_marked: GMarked = marked.into();
        self.set_property("marked", g_marked.to_value())
    }

    pub fn date(&self) -> GDateTime {
        *self.imp().date.borrow()
    }

    pub fn date_string(&self) -> String {
        self.imp().date_string.borrow().clone()
    }

    pub fn set_date(&self, date: NaiveDateTime) {
        self.set_property("date-string", DateUtil::format_time(&date));

        let g_datetime: GDateTime = date.into();
        self.set_property("date", g_datetime.to_value())
    }

    pub fn tags(&self) -> GTags {
        self.imp().tags.borrow().clone()
    }

    pub fn set_tags(&self, tags: Vec<Tag>) {
        let g_tags: GTags = tags.into();
        self.set_property("tags", g_tags.to_value())
    }

    pub fn thumbnail(&self) -> GThumbnail {
        self.imp().thumbnail.borrow().clone()
    }

    pub fn set_thumbnail(&self, thumbnail_url: Option<String>) {
        let g_thumbnail: GThumbnail = thumbnail_url.into();
        self.set_property("thumbnail", g_thumbnail.to_value())
    }

    pub fn feed_title(&self) -> String {
        self.imp().feed_title.borrow().clone()
    }

    pub fn set_feed_title(&self, feed_title: String) {
        self.set_property("feed-title", feed_title.to_value())
    }

    pub fn title(&self) -> Arc<String> {
        self.imp().title.borrow().clone()
    }

    pub fn summary(&self) -> Arc<String> {
        self.imp().summary.borrow().clone()
    }

    pub fn feed_id(&self) -> FeedID {
        self.imp().feed_id.borrow().clone()
    }

    pub fn article_id(&self) -> ArticleID {
        self.imp().id.borrow().clone()
    }

    pub fn set_selected(&self, selected: bool) {
        self.set_property("selected", selected.to_value())
    }

    pub fn is_selected(&self) -> bool {
        self.imp().selected.get()
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Enum)]
#[repr(u32)]
#[enum_type(name = "NewsFlashGRead")]
pub enum GRead {
    Read,
    Unread,
}

impl Default for GRead {
    fn default() -> Self {
        Self::Read
    }
}

impl From<Read> for GRead {
    fn from(read: Read) -> Self {
        match read {
            Read::Read => Self::Read,
            Read::Unread => Self::Unread,
        }
    }
}

impl From<GRead> for Read {
    fn from(read: GRead) -> Self {
        match read {
            GRead::Read => Read::Read,
            GRead::Unread => Read::Unread,
        }
    }
}

impl GRead {
    pub fn invert(&self) -> Self {
        match self {
            Self::Read => Self::Unread,
            Self::Unread => Self::Read,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Enum)]
#[repr(u32)]
#[enum_type(name = "NewsFlashGMarked")]
pub enum GMarked {
    Marked,
    Unmarked,
}

impl Default for GMarked {
    fn default() -> Self {
        Self::Unmarked
    }
}

impl From<Marked> for GMarked {
    fn from(marked: Marked) -> Self {
        match marked {
            Marked::Marked => Self::Marked,
            Marked::Unmarked => Self::Unmarked,
        }
    }
}

impl GMarked {
    pub fn invert(&self) -> Self {
        match self {
            Self::Marked => Self::Unmarked,
            Self::Unmarked => Self::Marked,
        }
    }
}

impl From<GMarked> for Marked {
    fn from(marked: GMarked) -> Self {
        match marked {
            GMarked::Marked => Marked::Marked,
            GMarked::Unmarked => Marked::Unmarked,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Boxed)]
#[boxed_type(name = "NewsFlashGDateTime")]
pub struct GDateTime(NaiveDateTime);

impl From<NaiveDateTime> for GDateTime {
    fn from(dt: NaiveDateTime) -> Self {
        Self(dt)
    }
}

impl From<GDateTime> for NaiveDateTime {
    fn from(dt: GDateTime) -> Self {
        dt.0
    }
}

impl From<GDateTime> for DateTime<Utc> {
    fn from(dt: GDateTime) -> Self {
        dt.0.and_utc()
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Boxed)]
#[boxed_type(name = "NewsFlashGTags")]
pub struct GTags(Vec<Tag>);

impl From<Vec<Tag>> for GTags {
    fn from(dt: Vec<Tag>) -> Self {
        Self(dt)
    }
}

impl From<GTags> for Vec<Tag> {
    fn from(dt: GTags) -> Self {
        dt.0
    }
}

impl<'a> From<&'a GTags> for &'a [Tag] {
    fn from(dt: &'a GTags) -> Self {
        &dt.0
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Boxed)]
#[boxed_type(name = "NewsFlashGThumbnail")]
#[derive(Default)]
pub struct GThumbnail(Option<String>);

impl From<Option<String>> for GThumbnail {
    fn from(tn: Option<String>) -> Self {
        Self(tn)
    }
}

impl From<GThumbnail> for Option<String> {
    fn from(tn: GThumbnail) -> Self {
        tn.0
    }
}

impl GThumbnail {
    pub fn has_value(&self) -> bool {
        self.0.is_some()
    }
}
